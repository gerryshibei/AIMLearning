# -*- coding: utf-8 -*-
__author__ = 'gerry'


import matplotlib.pyplot as plt
import  numpy as np

import  csv as csv
import  sys

#获取并修改参数
#mpl.rcParams['lines.linewidth'] =2
#mpl.rcParams['lines.color'] = 'r'

#mpl.rc('lines',linewidth=2,color='r')

t = np.arange(0.0,1.0,0.01)
s = np.sin(2*np.pi*t)
#make line red

plt.rcParams['lines.color']='r'
plt.plot(t,s)

c = np.cos(2*np.pi*t)
#make line thick
plt.rcParams['lines.linewidth']='3'
plt.plot(t,c)
plt.show()

#重置设置文件
#help(mpl.rcdefaults)

#读取CSV数据

'''
filename = 'ch02-data.csv'

data = []
try:
    with open(filename) as f:
        reader = csv.reader(f)
    header = reader.next()
    data = [row for row in reader]
except csv.Error as e:
    print "Error reading CSV file at line %s:%s" %(reader.line_num,e)
    sys.exit(-1)

if header:
    print header
    print '========================'
for datarow  in data:
    print datarow
'''

# 读取大文件

data1 = np.loadtxt('ch02-data.csv',dtype='string',delimiter=',')

print data1


# 读取excel文件

import xlrd
















