# -*- coding: utf-8 -*-
__author__ = 'gerry'

import numpy as np

#1.2.3矢量化编程与GPU运算

#矢量化编程：
# 基于矩阵的算法都是针对向量的，也称为矢量，为了简化的逻辑，需要一种新的编程方法，处理基于运算，这就是所谓的矢量化编程

#矢量化编程的一个重要的特点就是可以直接将数学公式转换为相应的程序代码

#demo1
mylist = [1,2,3,4,5]
length = len(mylist)
a = 10
for index  in xrange(length):
    mylist[index] = a+mylist[index]
print mylist
#demo2

mymatrix = np.mat(mylist)
print a*mymatrix


#1.2.4 理解数学公式与Numpy矩阵运算

#-->阵的初始化

myZero = np.zeros([3,5]) #全0矩阵
print myZero
myOnes = np.ones([3,3]) #全1矩阵
print myOnes

#*生成随机矩阵
myRand = np.random.rand(3,4) #3行4列的0~1之间的随机矩阵
print myRand

#*单位矩阵
myEye = np.eye(3, dtype=np.int32) #单位矩阵
print myEye

#-->矩阵的元素运算

#*元素相加和相减
print myOnes+myEye #矩阵相加
print myOnes-myEye #矩阵相减

#*矩阵的数乘
mymatrix1 = np.mat([[1,2,3],[2,3,4],[3,4,5]])
a1 = 10
print a*mymatrix1
#*矩阵所有元素的和
mymatrix2 =np.mat([[1,2,3],[2,3,4],[3,4,5]])
print np.sum(mymatrix2)

#*矩阵各个元素的积：矩阵的点乘同维对应元素的相乘。当矩阵的维度不同时，会根据一定的广播规则将维数扩充到一致的形式
mymatrix3 = 1.5*np.ones([3,3])
print np.multiply(mymatrix1,mymatrix3)

#*矩阵各个元素的n次幂：n=2
print np.power(mymatrix1,2)



#-->矩阵的乘法：矩阵乘以矩阵

mymatrix4 = np.mat([[1],[2],[3]])
print mymatrix1*mymatrix4

#-->矩阵的转置
print mymatrix1.T #矩阵的转置
mymatrix1.transpose() #矩阵的转置
print mymatrix1
#-->矩阵的其他操作：行列数、切片、复制、比较
[m,n] = np.shape(mymatrix1) #矩阵的行列数
print "矩阵的行列数：",m,n
myscl1 = mymatrix1[0] #按行切片
print "按行切片：",myscl1

myscl2 = mymatrix1.T[0] #按列切片
print "按列切片：",myscl2

mycpmat = mymatrix1.copy()#矩阵的复制
print "矩阵的复制：\n",mycpmat

#比较
print "矩阵元素的比较：",mymatrix1<mymatrix2










































