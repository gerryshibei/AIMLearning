# -*- coding: utf-8 -*-
__author__ = 'gerry'

from numpy import *


import scipy.spatial.distance as dist  # 导入scipy距离公式

# Linalg线性代数库

# 矩阵的行列式

A = mat([[1, 2, 4, 5, 7], [9, 12, 11, 8, 2], [9, 1, 3, 2, 1], [9, 1, 3, 4, 5], [0, 2, 3, 4, 1]])
print "det(A):", linalg.det(A)  # 矩阵的行列式

# 矩阵的逆
invA = linalg.inv(A)  # 矩阵的逆
print "inv(A):", invA

# 矩阵的对称
AT = A.T
print "矩阵的对称A*AT:", A * AT

# 矩阵的秩

print "矩阵的秩：", linalg.matrix_rank(A)
# 可以矩阵的解
b = [1, 0, 1, 0, 1]
S = linalg.solve(A, b)
print S

print "=============================================="
# 向量的范数：可以简单的理解为向量的长度，或者向量到坐标系原点的距离或者相应空间内两点之间的距离
# 向量的范数定义：向量的范数是一个函数||x||,满足非负性||x||>=0,其次性||cx||=|c|*||x||,三角不等性||x+y||<=||x||+||y||
# L1范数：||x||为x向量各个元素绝对值之和
# L2范数:||x||为x向量各个元素平方和的开方
# Lp范数：||x||为x向量各个元素绝对值的p次方和的1/p次方
# L无穷大范数：limit(sum(|pi-qi|^k))^1/k
A = [8, 1, 6]
# 手工运算
modA = sqrt(sum(power(A, 2)))
print "modA:", modA

# 库函数
normA = linalg.norm(A, 8)
print "normA:", normA

print "=============================================="
# 各类距离
# 闽科夫斯基距离：不是一种距离，而是一组距离的定义
# A(x11,x12,x13,...x1n)与B(x21,x22,x23....x2k)
# d12 = (sum((x1k-x2k)^p))^1/p
# p=1,是曼哈顿距离
# p=2,是欧氏距离
# p->∞就是切比雪夫距离
# 欧式距离（L2范数）：d12 = sqrt((A-B)(A-B)')
vector1 = mat([1, 2, 3])
vector2 = mat([4, 5, 6])
print "欧式距离：", sqrt((vector1 - vector2) * (vector1 - vector2).T)
# 曼哈顿距离（L1范数）：d12 = sum(|x1k-x2k|)
print "曼哈顿距离:", sum(abs(vector1 - vector2))
# 切比雪夫距离（L∞范数）：d12 = (sum((x1k-x2k)^p))^1/p<==>da12 = max(|x1k,x2k|)
print "切比雪夫距离：", abs(vector1 - vector2).max()
# 夹角余弦：衡量两个样本点的相似程度
# cosθ=AB/|A||B|<==>cosθ=sum(x1kx2k)/(sqrt(sum(x1k^2))*sqrt(sum(x2k^2)))
vector11 = [1,2,3]
vector22 = [3,4,5]
cosV12 = dot(vector11, vector22) / (sqrt(dot(vector11,vector11)) *sqrt(dot(vector22,vector22)) )  # dot向量的点积：用处就是计算两个向量之间的夹角
matX = mat([vector11,vector22])
print "夹角余弦1：",dist.pdist(matX,'cosine')
print "夹角余弦：", cosV12
# 杰卡德相似系数：两个集合A和B的交集元素在A、B的并集中所占的比例
# J(A,B) = |A∩B|/|A∪B|:是衡量两个集合相似度的一种指标
# 杰卡得距离Jδ(A,B) = 1-J(A,B)=(|A∪B|-|A∩B|)/|A∪B|:用两个集合中不同元素占所有元素的比例来衡量两个集合的区分度

matV = mat([[1, 1, 0, 1, 0, 1, 0, 0, 1], [0, 1, 1, 0, 0, 0, 1, 1, 1]])
print "dist.jaccard:", dist.pdist(matV, 'jaccard')
#Y = pdist(X, 'euclidean'):欧式距离
#Y = pdist(X, 'minkowski', p)：Minkowski距离计算距离
#Y = pdist(X, 'cityblock')：曼哈顿距离
#Y = pdist(X, 'seuclidean', V=None)：标准欧式距离
#Y = pdist(X, 'sqeuclidean')：欧氏距离的平方
#Y = pdist(X, 'cosine')：计算余弦距离
#Y = pdist(X, 'correlation')：计算相关距离
#Y = pdist(X, 'hamming')：计算汉明距离
#Y = pdist(X, 'jaccard')：计算杰卡得距离
#Y = pdist(X, 'chebyshev')：计算切比雪夫距离
#Y = pdist(X, 'canberra')：计算堪培拉距离
























