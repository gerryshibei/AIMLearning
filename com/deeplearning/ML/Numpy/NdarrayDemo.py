# -*- coding: utf-8 -*-
__author__ = 'gerry'

import numpy as np



# 多维数组
# 直接使用嵌套列表
a = np.array([[1,2,3,4],[2,3,4,5]])
print a

# ndim:维度数量

b = np.ndim(a)
print b,type(b)

# shape:行列值,各维数组的维数
c = np.shape(a)
print c,type(c)
# 数组的数据类型设置
d = np.dtype
print d,type(d)

arr1 = np.array(['Python','CCTV','Hello worlfd'],dtype='|S4')
print arr1

arr2 = np.array(['1','2','3','4'],dtype='float')
print arr2
# size:数组中的元素个数

num = np.size(a)
print num


# shape的巧妙运算

arr3 = np.array([
    [
        [1,2,3,4,5],
        [2,3,4,5,6],
        [4,5,6,7,7]
    ],
    [
        [1,2,3,4,5],
        [2,3,4,5,6],
        [4,5,6,7,7]
    ]


])
print arr3.shape,type(arr3.shape)
print arr3[0][1][1]

# array:接收一个普通的Python序列，转成ndarray
# zeros函数

# help(np.zeros)

arr4 = np.zeros((2,2),dtype=int)
print(arr4)

# ones函数

#help(np.ones)

arr5 = np.ones(5)
print arr5


arr6 = np.ones((5,5),dtype=int)
print arr6
# empty函数:填充随机值
#help(np.empty)

arr7 = np.empty([2, 2])
print(arr7)

# arange函数：类似于Python的range函数，通过指定开始值、终值和步长创建一维数组

#help(np.arange)

arrge = range(2,20,3)
arrge1 = np.arange(2,20,3)
print arrge,arrge1


# linspace函数：通过指定开始值、终值和元素个数来创建一维数组(等差数列)，可以通过endpoint关键字指定是否包括终值，缺省值设置是包括终值

#help(np.linspace)
linsp = np.linspace(2,20,3,endpoint=True)
print(linsp)




# logspace函数：和linspace类似，不过他创建等比数列
#help(np.logspace)
# 使用随机数填充数组，即使用numpy.random模块的random()函数，数组所包含的元素数量由参数决定

arrge3 = np.arange(0,12).reshape(3,4)
print(arrge3)

#help(np.random)

rand1 = np.random.random((2,3))
print(rand1)



#Numpy中的数据类型
#创建NumPy数组时可以通过dtype属性显式指定数据类型，如果不指定Numpy会自己推断出合适的数据类型，所以一般无需显式指定
#astype,可以转换数组元素的数据类型，得到一个新数组

adarray01 = np.array([1,2,3,4,5])
print adarray01.dtype


adarray02 = adarray01.astype(float)
print adarray02.dtype


arr01 = adarray01.astype('|U2') #使用unicode编码，每个元素长度为2
print arr01

arr02 = adarray01.astype('|S2')#使用String进行编码（转换为十六进制编码），每个元素长度为2
print arr02

arr03 = np.array([1,2,3,'4'],dtype=np.float)
print arr03

# 改变形状
#help(np.reshape)
# * reshape函数不会改变原来的ndarray,但是得到的新的ndarray是原数组的视图（视图：不同变量指向同一个地址，一个发生改变，另一个也会发生改变）
# *副本，是对原来内容的拷贝，放到新的内存地址,改变一个不会影响另外一个的值
arr04 = np.arange(20)
arr05 = np.reshape(arr04,(4,5))
print arr05

arr06 = arr04.copy()
print arr06

# 对于ndarry的一些方法，首先要区分的是是否会改变原来的变量，以此来判断视图还是副本










































