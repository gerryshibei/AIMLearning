# -*- coding: utf-8 -*-
__author__ = 'gerry'

from numpy import *
from numpy import random as nr
'''
    Numpy庞大的函数库
'''
# 1、随机数
# rand:0-1之间的随机数
# randn:标砖正太分布的随机数
# randint:指定范围内的随机整数（包括起始值，不包括终值）
# normal:正太分布,前两个参数分别为期望值和标准差
# uniform:均匀分布,前两个参数分别为区间的起始值和终值
# poisson：泊松分布，第一个参数指定λ系数，他表示单位时间内随机事件的平均发生率，由于泊松分布式一个离散分布，因此它输出的数组时一个整数数组
# permutation:随机排列（乱序），当参数为整数时n时，它返回[0,n)这n个整数的随机排列，当参数是一个序列时，它返回一个随机排列后的序列。
# shuffle：随机打乱顺序
# choice:随机抽取样本
# seed:设置随机种子

set_printoptions(precision=2) #显示小数点后两位数字
r1 = nr.rand(4,3)
r2 = nr.randn(4,3)
r3 = nr.randint(0,10,(4,3))
print r1
print r2
print r3

r4 = nr.normal(100,10,(4,3))
r5 = nr.uniform(10,20,(4,3))
r6 = nr.poisson(2.0,(4,3))
print r4
print r5
print r6

r7 = array([10,10,20,30,40])
print nr.permutation(10)
print nr.permutation(r7)

print nr.shuffle(r7)

















































