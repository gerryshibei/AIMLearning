# -*- coding: utf-8 -*-
__author__ = 'gerry'

from numpy import *
import cv2
from matplotlib import pyplot as plt

# OpenCV读取保存图片
win_name = 'mypicture'  #窗口名称
cv2.namedWindow(win_name,cv2.WINDOW_NORMAL)
img = cv2.imread('image\snapshot0001.jpg',1)#0为黑白图片，1为原色图片
cv2.imshow(win_name,img) #显示图片
cv2.waitKey(0)
cv2.imwrite("cat2.jpg", img) #保存图片
#图片的复制
emptyImage = zeros(img.shape,uint8) #创建图像
emptyImage2 = img.copy #图像的渎职

cv2.destroyAllWindows() #销毁创建的对象

# 在Matplotlib中显示图像
img1 = cv2.imread('image\paulwalker.mono.pgm',0) #读取黑白图像
plt.imshow(img,cmap = 'gray',interpolation = 'bicubic')
plt.xticks([])
plt.yticks([]) #隐藏x,y坐标
plt.show()

# 绘制直线和矩形
img2 = zeros((512,512,3))
#起点:(0,0).终点(511,511):颜色:(0,255,255);像素:2
cv2.line(img2,(0,255),(512,255),(0,255,255),2) #直线
#line(img, pt1, pt2, color, thickness=None, lineType=None, shift=None)
#左上角:(150,150);右下角:(350,350);颜色:(0,255,255);像素：2
cv2.rectangle(img2,(150,150),(350,350),(255,255,0),2) #矩形
#rectangle(img, pt1, pt2, color, thickness=None, lineType=None, shift=None)
cv2.imshow('image',img2)
cv2.waitKey(0)
cv2.destroyAllWindows()


#绘制圆和椭圆
img3 = zeros((512,512,3))
#绘制圆：圆心(255,255),半径60，颜色(0,0,255),像素1
cv2.circle(img3,(255,150),60,(0,255,255),2) #圆
#circle(img, center, radius, color, thickness=None, lineType=None, shift=None)
#绘制椭圆
cv2.ellipse(img3,(255,350),(100,50),0,0,360,(255,255,0),2) #椭圆
#ellipse(img, center, axes, angle, startAngle, endAngle, color, thickness=None, lineType=None, shift=None)
cv2.imshow('image1',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()

img4 = zeros((512,512,3))
#多边形顶点坐标
pts = array([[50,100],[150,150],[170,120],[250,210],[250,310]])
cv2.polylines(img4,[pts],True,(0,255,255),2) #True表示封口
cv2.imshow('image2',img4)
cv2.waitKey(0)
cv2.destroyAllWindows()


#写入文字
img5 = zeros((512,512,3))
font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img5,"Face Cognition",(10,255),font,2,(255,255,255),2)
cv2.imshow('image4',img5)
cv2.waitKey(0)
cv2.destroyAllWindows()

# 人脸识别的内容
# 贝尔胡米尔等提出了Fisherface人脸识别方法，该方法首先采用主成分分析（PCA，即特征脸）对图像表现特征进行降维。
#在此基础上，采用线性判别分析（LDA）的方法变换降维后的主成分以期获得“尽量大的类间散度和尽量小的类内散度”
#该方法目前仍然是主流的人脸识别方法之一

#人脸识别包含模块：
    #大模块：人脸检测（人脸跟踪、人脸检测）->关键点提取（关键点、角度估计）->人脸对齐、人脸规整（3D矫正、光照规整）->
    #->人脸特征、人脸分类、识别策略（年龄识别、性别识别、表情识别、活体识别、人脸比对、人脸识别）
#人脸检测原理与Haar级联检测
#在实践中，人脸检测主要用于人脸识别的预处理，即在图片或图像中准确的标定出人脸所处的位置和大小。即人脸检测需要挑选出人脸图像中所包含的各种模式，其中有直方图特征、颜色特征、模板特征、结构特征以及Haar特征，然后根据某种规则，通过算法作出判定





































