# _*_coding:utf-8_*_
__author__ = 'gerry'
'''replace the string'''

# import sys
# if '-r' in sys.argv:
#     rep_argv_pos = sys.argv.index('-r')
#     find_str = sys.argv[rep_argv_pos+1]
#     new_str = sys.argv[rep_argv_pos+3]

find_str = "man"
new_str = "alex"
f = file('test.txt', 'r+')
while True:
    line = f.readline()
    if find_str in line:
        # print '-->cur pos:', f.tell(), len(line)
        last_line_pos = f.tell() - len(line)
        f.seek(last_line_pos)
        # print '-->cur pos:', f.tell()
        new_line = line.replace(find_str, new_str)
        # print new_line
        f.write(new_line)
        f.flush()
f.close()

exit()
