# _*_coding:utf-8_*_


import numpy as np
import operator
from os import listdir
import matplotlib.pyplot as plt

'''
*计算一只类别数据集中的点和当前点的距离
*按照距离递增次序排序
*选取当前距离最小的的K个点
*确定前K个点所在类别的出现频率
*返回前k个点出现频率最高的类别作为当前点的预测分类
'''

#KNN聚类方法
def classify0(inX, dataSet, labels, k):
    dataSetSize = dataSet.shape[0]
    # 距离计算,
    # tile:生成和dataSet相同维数的样本数据相同的数据集矩阵
    diffMat = np.tile(inX, (dataSetSize, 1)) - dataSet
    sqDiffMat = diffMat ** 2
    sqDistance = sqDiffMat.sum(axis=1)  # 按照列进行相加
    distance = sqDistance ** 0.5
    print distance
    sortedDistIndicies = distance.argsort()
    classCount = {}
    print sortedDistIndicies
    for i in range(k):
        voteIlabel = labels[sortedDistIndicies[i]]
        classCount[voteIlabel] = classCount.get(voteIlabel, 0) + 1  # 选择距离最小的点
    sortedClassCount = sorted(classCount.iteritems(), key=operator.itemgetter(1), reverse=True)

    print sortedClassCount
    return sortedClassCount[0][0]

#数据预处理
def file2matrix(filename):
    fr = open(filename)
    numberOfLines = len(fr.readlines())  # get the number of lines in the file
    returnMat = np.zeros((numberOfLines, 3))  # prepare matrix to return
    classLabelVector = []  # prepare labels return
    fr = open(filename)
    index = 0
    for line in fr.readlines():
        line = line.strip()
        listFromLine = line.split('\t')
        returnMat[index, :] = listFromLine[0:3]
        label_value = 0
        if listFromLine[-1] == 'largeDoses':
            label_value = 3
        elif listFromLine[-1] == 'smallDoses':
            label_value = 2
        elif listFromLine[-1] == 'didntLike':
            label_value = 1
        classLabelVector.append(label_value)
        index += 1
    return returnMat, classLabelVector


def pictureScatter():
    datingDataMat,datingLabels = file2matrix('datingTestSet.txt')
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(datingDataMat[:,1],datingDataMat[:,2],15.0*np.array(datingLabels),15.0*np.array(datingLabels))
    plt.show()


## 归一化数值
def autoNorm(dataSet):
    minVals = dataSet.min(0)
    maxVals = dataSet.max(0)
    ranges = maxVals-minVals
    normDataSet = np.zeros(np.shape(dataSet))
    m = dataSet.shape[0]
    normDataSet = dataSet-np.tile(minVals,(m,1))
    normDataSet = normDataSet/np.tile(ranges,(m,1))
    return normDataSet,ranges,minVals

# 进行分类
def datingClassTest():
    hoRatio = 0.1
    datingDataMat,datingLabels = file2matrix('datingTestSet.txt')
    normMat,ranges,minVals = autoNorm(datingDataMat)
    m = normMat.shape[0]
    numTestVecs = int(m*hoRatio)
    errorCount = 0
    for i in range(numTestVecs):
        classifierResult = classify0(normMat[i,:],normMat[numTestVecs:m,:],datingLabels[numTestVecs:m],3)
        print("the classifier came back with:%d,the real answer is: %d" %(classifierResult,datingLabels[i]))
        if classifierResult != datingLabels[i]:errorCount +=1.0
    print("the total error rate is :%f"%(errorCount/float(numTestVecs)))

# 约会网站预测函数
def classifyPerson():
    resultList = ['not at all','in small does','in large doses']
    percentTats = float(raw_input("percentage of time spent palying video games?"))
    ffMiles = float(raw_input("frequent filer miles earned per years?"))
    iceCream = float(raw_input("liters of ice cream consumed per year?"))
    datingDataMat,datingLabels = file2matrix('datingTestSet2.txt')
    normMat,ranges,minVals = autoNorm(datingDataMat)
    inArr = np.array([ffMiles,percentTats,iceCream])
    classifierResult = classify0(inArr,normMat,datingLabels,3)
    print("You will probably like this person:",resultList[classifierResult-1])




#将图像转换为测试向量
def img2vector(filename):
    returnVect = np.zeros((1,1024))
    fr = open(filename)
    for i in range(32):
        lineStr = fr.readline()
        for j in range(32):
            returnVect[0,32*i+j] = int(lineStr[j])
    return returnVect


#手写数字识别系统的测试代码
def handwriterClassTest():
    hwLabels = []
    #获取目录内容
    trainingFileList = listdir('trainingDigits')
    m = len(trainingFileList)
    trainingMat = np.zeros((m,1024))
    for i in range(m):
        fileNameStr = trainingFileList[i]
        fileStr = fileNameStr.split('.')[0]
        classNumStr = int(fileStr.split('_')[0])
        hwLabels.append(classNumStr)
        trainingMat[i,:] = img2vector('trainingDigits/%s'%fileNameStr)
    testFiledList = listdir('testDigits')
    errorCount = 0.0
    mTest = len(testFiledList)
    for i in range(mTest):
        fileNameStr = testFiledList[i]
        fileStr = fileNameStr.split('.')[0]
        classNumStr = int(fileStr.split('_')[0])
        vectorUnderTest = img2vector('testDigits/%s'%fileNameStr)
        classifierResult = classify0(vectorUnderTest,trainingMat,hwLabels,3)
        print("the classifier came back with；%d,the real answer is ：%d"%(classifierResult,classNumStr))
        if (classifierResult!=classNumStr):errorCount+=1.0
    print("\nthe total number of errors is:%d"%errorCount)
    print("\nthe total errpr rate is:%f"%(errorCount/float(mTest)))













def createDataSet():
    group = np.array([[1.0, 1.1], [1.0, 1.0], [0, 0], [0, 0.1]])
    labels = ['A', 'A', 'B', 'B']
    return group, labels


if __name__ == '__main__':
    #datingClassTest()
    #classifyPerson()
    handwriterClassTest()














