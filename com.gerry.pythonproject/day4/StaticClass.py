# _*_coding:utf-8_*_
__author__ = 'gerry'

class MyClass(object):

    age = 22

    def __init__(self):
        self.name = "alex"
    def sayh1(self):
        print "----asyhi 1"
    @staticmethod#静态方法，跟类没有什么关系,不需要实例化就可以调用，类的工具包
    def sayh2():
        print "----asyhi 2"
    @classmethod #类方法：不需要实例化即可调用，不能访问实例数据,能够访问类变量，不能访问实例变量
    def sayh3(self):
        print "----asyhi 3",self.age
    @property #把一个方法作为一个静态属性，可以访问类的实例变量
    def sayh4(self):
        print "----asyhi 4",self.age,self.name
        return 'test'

m = MyClass()
m.sayh2()
MyClass.sayh2()
m.sayh1()
m.sayh3()
print m.sayh4



















