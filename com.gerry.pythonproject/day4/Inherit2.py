# _*_coding:utf-8_*_
__author__ = 'gerry'


class SchoolMember(object):
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def tell(self):
        print '''
                --info of %s --
                name:%s
                age:%s
                sex:%s
        ''' % (self.name, self.name, self.age, self.sex)

class School(object):
    def __init__(self, school_name, addr, tel):
        self.school_name = school_name
        self.addr = addr
        self.tel = tel
        self.stu_list = []
        self.tech_list = []

class Student(SchoolMember,School):  # 多继承
    def __init__(self, name, age, sex, grade):
        SchoolMember.__init__(self, name, age, sex)  # 调用父类的构造函数
        School.__init__(self,"Oldboy","Shashel",999)
        self.grade = grade

    def pay_money(self):
        print "----%s is paying the tution fee---" % self.name

    def tell(self):
        SchoolMember.tell(self)
        print '''---from school name:%s
                class:%s
                addr :%s
                ''' % (self.school_name, self.grade, self.addr)


class Teacher(SchoolMember,School):
    def __init__(self, name, age, sex, course, salary):
        SchoolMember.__init__(self, name, age, sex)
        School.__init__(self,"Oldboy","Shashel",999)
        self.cource = course
        self.salary = salary

    def teaching(self):
        print("Teacher %s is teaching class of %s" % (self.name, self.cource))






school = School("oldBoy", "ShaHe", 999)
school1 = School("xiongdil", "jhju", 79879)

s = Student("wang", 33, "M", "2班", school)
t = Student("School", 23, "S", "3班级", school1)
s.tell()
t.tell()

school.stu_list = [s,t]
