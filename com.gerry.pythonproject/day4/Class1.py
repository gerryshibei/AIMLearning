# _*_coding:utf-8_*_
__author__ = 'gerry'


class People(object):

    info = "test0"
    info_dict = {
        1:"gerry",
        2:"shibeibei"
    }
    def __init__(self,name,age,job,info):#构造函数
        self.__name = name #变成实例变量
        self.__age = age
        self.__job = job
        self.__info = info




    def get_info(self,info_type):#封装私有化方法
        if info_type =="age":
            return self.__age
        elif info_type =="job":
            return self.__job
        elif info_type =="name":
            return self.__name
    def __del__(self):#解构函数，一般不用
        pass









    def __breath(self):
        print("this is a self method!")
    def walk(self):
        print "I am working...",self.name,self.info
    def talk(self):
        print "talking with sb",self.age,self.info


p1 = People("gerry",29,"baoan","test2")#实例
p1.talk()
p1.walk()
p1.info_dict[1] ="nihao"


p2 = People("huanwen",22,"Baomu","test2")

p2.walk()
p2.talk()

print(People.info)

