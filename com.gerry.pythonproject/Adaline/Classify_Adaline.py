# _*_coding:utf-8_*_

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import AdalineGD
from matplotlib.colors import ListedColormap

def plot_decision_regions1(X, y, classifier, resolution=0.02):
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')

    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')

    cmap = ListedColormap(colors[:len(np.unique(y))])
    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    # plot classsamples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1], alpha=0.8, c=cmap(idx), marker=markers[idx], label=cl)
    return


def error_plot(X,y):
    X_std = np.copy(X)
    X_std[:,0] = (X[:,0]-X[:,0].mean())/X[:,0].std()
    X_std[:,1] = (X[:,1]-X[:,1].mean())/X[:,1].std()

    ada = AdalineGD.AdalineGD(n_iter =15,eta = 0.01)
    ada.fit(X_std,y)
    print(X_std)
    print(y)
    plot_decision_regions1(X_std,y,classifier=ada)
    plt.title('Adaline-Gradient Descent')
    plt.xlabel('sepal length [standardized]')
    plt.ylabel('petal length [standardized]')
    plt.legend(loc='upper left')
    plt.show()
    plt.plot(range(1,len(ada.cost_)+1),ada.cost_,marker = 'o')
    plt.xlabel('Epochs')
    plt.ylabel('Sum-squared-error')
    plt.show()
















if __name__ == '__main__':
    df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)

    row_len = df.shape[0]
    col_len = df.shape[1]
    y = df.iloc[0:row_len, 4].values
    y = np.where(y == 'Iris-setosa', -1, 1)
    X = df.iloc[0:row_len, [0, 2]].values

    fig, ax = plt.subplots(nrows=1,ncols=2,figsize = (8,4))
    ada1 = AdalineGD.AdalineGD(n_iter = 10,eta = 0.01).fit(X,y)
    ax[0].plot(range(1,len(ada1.cost_)+1),np.log10(ada1.cost_),marker ='o')
    ax[0].set_xlabel('Epochs')
    ax[0].set_ylabel('log(Sum-squared-error)')
    ax[0].set_title('Adaline-Learning rate 0.01')
    ada2 = AdalineGD.AdalineGD(n_iter = 10,eta = 0.01).fit(X,y)
    ax[1].plot(range(1,len(ada2.cost_)+1),ada2.cost_,marker ='o')
    ax[1].set_xlabel('Epochs')
    ax[1].set_ylabel('Sum-squared-error')
    ax[1].set_title('Adaline-Learning rate 0.001')

    plt.show()
    error_plot(X,y)






















