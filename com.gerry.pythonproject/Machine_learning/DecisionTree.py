# _*_coding:utf-8_*_
__author__ = 'gerry'

from sklearn.feature_extraction import DictVectorizer
import csv
from sklearn import preprocessing
from sklearn import tree
from sklearn import tree
from sklearn.externals.six import StringIO


#read in the csv file and put features in a list of dist
allMedicalData = open(r'Student.csv','rb') #read the data
reader  = csv.reader(allMedicalData)
headers = reader.next() #the name of row

featureList = []
labelList = []

for row in reader:
    labelList.append(row[len(row)-1])
    rowDist = {}
    for i in range(1,len(row)-1):
        rowDist[headers[i]] = row[i]
    featureList.append(rowDist)

#将原始数据转化为矩阵
vec = DictVectorizer()
dummyX = vec.fit_transform(featureList).toarray()

# print('dummyX:'+str(dummyX))
# print(vec.get_feature_names())

# vectorize class labels
lb = preprocessing.LabelBinarizer()
dummyY = lb.fit_transform(labelList)
#print('dummyY:'+str(dummyY))

#Using decision tree for classification
#clf = tree.DecisionTreeClassifier()
clf = tree.DecisionTreeClassifier(criterion='entropy')
clf = clf.fit(dummyX,dummyY)
#print('clf:'+str(clf))

with open("allMedicalData.dot",'w') as f:
    f = tree.export_graphviz(clf,feature_names=vec.get_feature_names(),out_file=f)

oneRowX = dummyX[0,:]
print("oneRowx:"+str(oneRowX))
newRow = oneRowX
newRow[0] =1
newRow[2] =0
print("newRowX:"+str(newRow))
predictedY = clf.predict(newRow)
print("predictedY"+str(predictedY))


















