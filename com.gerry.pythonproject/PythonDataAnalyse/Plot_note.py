# _*_coding:utf-8_*_

import matplotlib.pyplot as plt
import numpy as np


def pie_plot():
    labels = 'frogs', 'hogs', 'dogs', 'logs'
    sizes = 15, 20, 45, 10
    colors = 'yellowgreen', 'gold', 'lightskyblue', 'lightcoral'
    explode = 0, 0.1, 0, 0
    plt.pie(sizes, explode=explode, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=50)
    plt.axis('equal')

    plt.rcParams['font.sans-serif'] = ['Simhei']  # 用来显示中文标签
    plt.rcParams['axes.unicode_minus'] = False  # 用来显示负号
    plt.show()

    '''
    axex: 设置坐标轴边界和表面的颜色、坐标刻度值大小和网格的显示
    backend: 设置目标暑促TkAgg和GTKAgg
    figure: 控制dpi、边界颜色、图形大小、和子区( subplot)设置
    font: 字体集（font family）、字体大小和样式设置
    grid: 设置网格颜色和线性
    legend: 设置图例和其中的文本的显示
    line: 设置线条（颜色、线型、宽度等）和标记
    patch: 是填充2D空间的图形对象，如多边形和圆。控制线宽、颜色和抗锯齿设置等。
    savefig: 可以对保存的图形进行单独设置。例如，设置渲染的文件的背景为白色。
    verbose: 设置matplotlib在执行期间信息输出，如silent、helpful、debug和debug-annoying。
    xticks和yticks: 为x,y轴的主刻度和次刻度设置颜色、大小、方向，以及标签大小。
    '''
    '''
    线条相关属性标记设置
        用来该表线条的属性
        线条风格linestyle或ls	描述	线条风格linestyle或ls	描述
        '-'	实线  	':'	虚线
        '--'破折线	'None',' ',''	什么都不画
        '-.'点划线
    线条标记
        标记maker	描述	标记	描述
        'o'	圆圈	'.'	点
        'D'	菱形	's'	正方形
        'h'	六边形1	'*'	星号
        'H'	六边形2	'd'	小菱形
        '_'	水平线	'v'	一角朝下的三角形
        '8'	八边形	'<'	一角朝左的三角形
        'p'	五边形	'>'	一角朝右的三角形
        ','	像素	'^'	一角朝上的三角形
        '+'	加号	'\	'	竖线
        'None','',' '	无	'x'	X
    颜色
        可以通过调用matplotlib.pyplot.colors()得到matplotlib支持的所有颜色。
        别名	颜色	别名	颜色
        b	蓝色	g	绿色
        r	红色	y	黄色
        c	青色	k	黑色
        m	洋红色	w	白色
        如果这两种颜色不够用，还可以通过两种其他方式来定义颜色值：
        使用HTML十六进制字符串 color='eeefff' 使用合法的HTML颜色名字（'red','chartreuse'等）。
        也可以传入一个归一化到[0,1]的RGB元祖。 color=(0.3,0.3,0.4)
        很多方法可以介绍颜色参数，如title()。'''


def x_y_limit():
    x = np.arange(-5.0, 5.0, 0.02)
    y1 = np.sin(x)
    plt.figure(1)
    plt.subplot(211)
    # 设置x轴范围
    plt.xlim(-2.5, 2.5)
    plt.ylim(-1, 1)
    plt.plot(x, y1)
    # 叠加图
    plt.subplot(212)
    t = np.arange(0., 5., 0.2)
    plt.plot(t, t, 'r--', t, t ** 2, 'bs', t, t ** 3, 'g^')

    plt.figure(2)  # 第二张图
    plt.subplot(211)  # 第一张图的第一张子图
    plt.plot([1, 2, 3])
    plt.subplot(212)  # 第一张图的第二张子图
    plt.plot([4, 5, 6])
    plt.figure(3)  # 第三张图
    plt.plot([4, 5, 6])  # 默认创建subplot(111
    plt.show()


def zhifang():
    mu, sigma = 100, 15
    x = mu + sigma * np.random.randn(10000)
    # 数据直方图
    n, bins, patches = plt.hist(x, 50, normed=1, facecolor='g', alpha=0.75)
    plt.xlabel('Smarts')
    plt.ylabel('Probaility')
    # 添加标题
    plt.title('Histogram of IQ')
    # 添加文字
    plt.text(60, 0.025, r'$\mu=100,\ \sigma = 15$')
    plt.axis([40, 160, 0, 0.03])
    plt.grid(True)
    plt.show()


def xticks_plot():
    # 创建一个 8 * 6 点（point）的图，并设置分辨率为 80
    plt.figure(figsize=(8, 6), dpi=80)
    # 创建一个新的 1 * 1 的子图，接下来的图样绘制在其中的第 1 块（也是唯一的一块）
    plt.subplot(111)
    X = np.linspace(-np.pi, np.pi, 256, endpoint=True)
    C, S = np.cos(X), np.sin(X)
    # 绘制余弦曲线，使用蓝色的、连续的、宽度为 1 （像素）的线条
    plt.plot(X, C, "b-", lw=1.0,label='cosine')
    # 绘制正弦曲线，使用绿色的、连续的、宽度为 1 （像素）的线条
    plt.plot(X,S,"r-",lw =4.0,label='sine')
    plt.legend(loc='upper left')
    plt.axis([-4,4,-1.2,1.2])
    #设置轴记号
    plt.xticks([-np.pi,-np.pi/2,0,np.pi/2,np.pi],[r'$-\pi$',r'$-\pi/2$',r'$0$',r'$\pi/2$',r'$\pi$'])
    plt.yticks([-1,0,1]),[r'$-1$',r'$0$',r'$+$']
    plt.show()

def scatter_plot():
    from sklearn import datasets
    from sklearn import linear_model
    plt.style.use('ggplot')
    #Load data
    boston = datasets.load_boston()
    yb = boston.target.reshape(-1,1)
    xb = boston['data'][:,5].reshape(-1,1)
    #plot data
    plt.scatter(xb,yb)
    plt.xlabel('number of rooms')
    plt.ylabel('value of house/1000($)')
    #create linear regression object
    regr = linear_model.LinearRegression()
    regr.fit(xb,yb)
    #Plot outputs
    plt.scatter(xb,yb,color = 'black')
    plt.plot(xb,regr.predict(xb),color ='blue',lw = 3)
    plt.show()

if __name__ == '__main__':
    # pie_plot()
    # x_y_limit()
    #zhifang()
    #xticks_plot()
    scatter_plot()











