# -*- coding:utf-8 -*-

import numpy as np
from scipy.optimize import fsolve

a = np.array([2, 1, 3, 5])
print(a)
print(a[:3])
print(a.min())
a.sort()
b = np.array([[1, 2, 3], [4, 5, 6], [2, 4, 5]])
print b


def f(x):
    x1 = x[0]
    x2 = x[1]

    return [2 * x1 - x2 ** 2 - 1, x1 ** 2 - x2 - 2]


result = fsolve(f, [1, 1])
print(result)

from scipy import integrate


def g(x):
    return (1 - x ** 2) ** 0.5


pi_2, err = integrate.quad(g, -1, 1)
print(pi_2 * 2)
