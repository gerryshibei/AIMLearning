# _*_coding:utf-8_*_
__author__ = 'gerry'

# 餐饮销量数据统计量分析
from __future__ import print_function
import pandas as pd
import MySQLdb


def read_data():
    conn = MySQLdb.connect(host="127.0.0.1", port=3306, user="root", passwd="123456", db="python", use_unicode=True,
                           charset="utf8")
    cur = conn.cursor()
    catering_sale_df = pd.read_sql('select * from catering_sale', conn, index_col=u'日期')
    return catering_sale_df


def statistic_analyse():
    data = read_data()
    data = data[(data[u'销量'] > 400) & (data[u'销量'] < 5000)]  # 过滤异常数据
    statistics = data.describe()  # 保存基本统计量

    statistics.loc['range'] = statistics.loc['max'] - statistics.loc['min']  # 极差
    statistics.loc['var'] = statistics.loc['std'] / statistics.loc['mean']  # 变异系数
    statistics.loc['dis'] = statistics.loc['75%'] - statistics.loc['25%']  # 四分位数间距

    print(statistics)

if __name__=="__main__":
    read_data()
    statistic_analyse()