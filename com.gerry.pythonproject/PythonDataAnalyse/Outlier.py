# _*_coding:utf-8_*_
__author__ = 'gerry'

import pandas as pd
import MySQLdb
import matplotlib.pyplot as plt  # 导入图像库


def read_data():
    conn = MySQLdb.connect(host="127.0.0.1", port=3306, user="root", passwd="123456", db="python", use_unicode=True,
                           charset="utf8")
    cur = conn.cursor()
    catering_sale_df = pd.read_sql('select * from catering_sale', conn)
    return catering_sale_df


def Outliter():
    plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
    plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
    plt.figure()  # 建立图像
    p = read_data().boxplot()  # 画箱线图，直接使用DataFrame的方法
    x = p['fliers'][0].get_xdata()  # 'flies'即为异常值的标签
    y = p['fliers'][0].get_ydata()

    y.sort()  # 从小到大排序，该方法直接改变原对象
    for i in range(len(x)):
        if i > 0:
            plt.annotate(y[i], xy=(x[i], y[i]), xytext=(x[i] + 0.05 - 0.8 / (y[i] - y[i - 1]), y[i]))
        else:
            plt.annotate(y[i], xy=(x[i], y[i]), xytext=(x[i] + 0.08, y[i]))
    plt.show()

if __name__ == '__main__':
    read_data()
    Outliter()
