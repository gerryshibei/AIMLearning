# _*_coding:utf-8_*_
__author__ = 'gerry'

from numpy import *
import matplotlib.pyplot as plt

dist = mat([[0.1, 0.1], [0.9, 0.5], [0.9, 0.1], [0.45, 0.9], [0.9, 0.8], [0.7, 0.9], [0.1, 0.45], [0.45, 0.1]])
disp(dist)
m, n = shape(dist)
plt.figure()  # 绘图
plt.subplot(111)
plt.scatter(dist.T.tolist()[0], dist.T.tolist()[1], color='blue', marker='o')
xlist = []
ylist = []
for px, py in zip(dist.T.tolist()[0], dist.T.tolist()[1]):
    xlist.append([px])
    ylist.append([py])
    plt.annotate("(" + str(px) + "," + str(py) + ")", xy=(px, py))
plt.plot(xlist, ylist, 'r')
plt.show()
