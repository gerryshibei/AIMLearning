# _*_coding:utf-8_*_
from __future__ import division

__author__ = 'gerry'

users = [
    {"id": 0, "name": "Hero"},
    {"id": 1, "name": "Dunn"},
    {"id": 2, "name": "Sue"},
    {"id": 3, "name": "Chi"},
    {"id": 4, "name": "Thor"},
    {"id": 5, "name": "Clive"},
    {"id": 6, "name": "Hicks"},
    {"id": 7, "name": "Devin"},
    {"id": 8, "name": "Kate"},
    {"id": 9, "name": "Klein"},

]

friendShip = [
    (0, 1), (0, 2), (1, 2), (1, 3), (2, 3), (3, 4),
    (4, 5), (5, 6), (5, 7), (7, 8), (6, 8), (8, 9)
]

for user in users:
    user["friends"] = []

for i, j in friendShip:
    # this works because user[i] is the user whose id is i
    users[i]["friends"].append(users[j])  # add i as a friend of j
    users[j]["friends"].append(users[i])  # add j as a friend of i


def number_of_friends(users):
    """how many friends does_user_have?"""
    return len(users["friends"])


total_connection = sum(number_of_friends(user) for user in users)

num_users = len(users)
avg_connections = total_connection / num_users
num_friends_id = [(user["id"],number_of_friends(user)) for user in users]
user_friends_num = sorted(num_friends_id,key = lambda (user_id,num_friends):num_friends,reverse=True)

for user in user_friends_num:
    print(user)


# for index, user in enumerate(users):
#     print index + 1, "\t", user["id"], "\t", user["name"], "\t", user["friends"]














