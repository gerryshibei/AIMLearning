# _*_coding:utf-8_*_

from numpy import *

#rand:产生（0,1）之间均匀分布矩阵
#randn:产生标准正太分布的伪随机矩阵（均值为0，方差为1）
#randMat.I:产生矩阵的逆
#eye(n):产生n*n的单位矩阵
randMat = mat(random.randn(4,4))
invRandMat = randMat.I
a = invRandMat*randMat - eye(4)
disp(a)





























